<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage specialty_veneers
 * @since 1.0
 * @version 1.2
 */

?>

<?php
    $footer = get_field('show_contact_form');
    if($footer || is_single()) {
?>
    <div class="contact-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10 col-xl-8">
                    <h1>Contact</h1>
                    <?php the_field('contact_form', 'option'); ?>
                    <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" tabindex="5"]'); ?>
                </div>
            </div>
        </div>
    </div>
<?php
    } else {
    	echo '<div class="no-contact"></div>';
    }
?>


    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="container">
            <div class="row">
                <div class="col-12 d-md-none mobilecontact">
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-2">
                            <i class="icon-location-outline contact"></i>
                        </div>
                        <div class="col-11 col-sm-10">
                            <?php the_field('address', 'option'); ?><br >
                            <a href="https://maps.google.com.au/?q=<?php echo urlencode(strip_tags(get_field('address', 'option'))); ?>">See on map <i class="icon-right"></i></a>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-2">
                            <i class="icon-phone-outline contact"></i>
                        </div>
                        <div class="col-11 col-sm-10">
                            <a href="tel:<?php the_field('phone', 'option'); ?>"><?php the_field('phone', 'option'); ?></a>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-2">
                            <i class="icon-mail-outlines contact"></i>
                        </div>
                        <div class="col-11 col-sm-10">
                            <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <h2>Mailing List</h2>
                    <p>Get the latest Specialty Veneer product information, price lists and special offers.</p>
                    <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true" tabindex="10"]'); ?>
                </div>
                <div class="col-12 col-md-6 col-lg-3 d-none d-md-block">
                    <h2>Get in touch</h2>
                    <p><?php the_field('address', 'option'); ?></p>
                    <p><a href="tel:<?php the_field('phone', 'option'); ?>"><?php the_field('phone', 'option'); ?></a></p>
                    <p><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></p>
                </div>
                <div class="col-12 col-md-8 col-lg-5">
                    <h2>Supplier of Certified Veneers</h2>
                    <?php $veneers = get_field('certified_veneers', 'option'); ?>
                    <?php if($veneers) { ?>
                        <img src="<?php echo $veneers['url']; ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="row site-by">
                <div class="col-12 col-md-4 text-center text-md-left">
                    <?php the_field('copyright_text', 'option'); ?>
                </div>
                <div class="col-6 col-md-4 text-left text-md-center">
                	<?php
                		$insta = get_field('instagram_url', 'option');
                		$fburl = get_field('facebook_url', 'option');
                		if($fburl) {
                			echo '<a href="'.$fburl.'" class="social">
                                <i class="icon-facebook" style="font-size: 20px; margin-right: 16px;"></i>
                            </a>';
                		}
                		if($insta) {
                			echo '<a href="'.$insta.'" class="social">
                                <i class="icon-instagram" style="font-size: 20px;"></i>
                            </a>';
                		}
                	?>
                </div>
                <div class="col-6 col-md-4 text-right">
                    Site by <a href="https://roundtablestudio.com.au"><i class="icon-rt-logo"></i></a>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>

</body>
</html>
