<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php
    require_once('nav-walker.php');

    $icon = get_field('site_logo', 'option');

    echo '<nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container">
            <a class="navbar-brand" href="'.get_site_url().'"><img src="'.$icon['url'].'"></a>
            <div class="buttons">
                <button class="navbar-toggler navbar-toggler-right" type="button" id="contactButton">
                    <i class="icon-mail-outlines"></i>
                </button>                    
                <button class="navbar-toggler navbar-toggler-right" type="button" id="searchButton">
                    <i class="icon-search"></i>
                </button>
                <button class="navbar-toggler navbar-toggler-right" type="button" id="menuButton">
                    <i class="icon-menu-1"></i>
                </button>
            </div>
            ';

    echo '<div class="collapse navbar-collapse" id="sv-menu">';
    echo '<div id="menu-outer" class="ml-auto">';

    wp_nav_menu([
     'menu'            => 'top',
     'theme_location'  => 'top',
     'container'       => 'div',
     'container_id'    => 'menunav',
     'container_class' => '',
     'menu_id'         => false,
     'menu_class'      => 'navbar-nav ml-auto',
     'walker'          => new WP_Bootstrap_Navwalker()
    ]);

    echo '<div id="contactnav" class="d-lg-none">
        <div class="col-12 d-lg-none mobilecontact">
            <div class="row align-items-center">
                <div class="col-1 col-sm-2">
                    <i class="icon-location-outline contact"></i>
                </div>
                <div class="col-11 col-sm-10">
                    '.get_field('address', 'option').'<br >
                    <a href="https://maps.google.com.au/?q='.urlencode(strip_tags(get_field('address', 'option'))).'">See on map <i class="icon-right"></i></a>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-1 col-sm-2">
                    <i class="icon-phone-outline contact"></i>
                </div>
                <div class="col-11 col-sm-10">
                    <a href="tel:'.get_field('phone', 'option').'">'.get_field('phone', 'option').'</a>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-1 col-sm-2">
                    <i class="icon-mail-outlines contact"></i>
                </div>
                <div class="col-11 col-sm-10">
                    <a href="mailto:'.get_field('email', 'option').'">'.get_field('email', 'option').'</a>
                </div>
            </div>
        </div>
        <div class="col-12">
            '.do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" tabindex="20"]').'
        </div>
    </div>';

    echo '<div id="searchnav" class="d-lg-none">
        <div class="row h-100 align-items-center">
            <div class="col">
                <h2>Search</h2>
                <h3>'.get_field('search_text', 'option').'</h3>
                <form role="search" method="get" class="search-form" action="'.get_site_url().'">
                    <input type="search" class="search-field" value="" name="s">
                    <button type="submit" class="search-submit">
                    </button>
                </form>
            </div>
        </div>        
    </div>';

    echo '</div>
        </div>
      </div>
    </nav>  
    <div id="desk-search" class="d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-12">
                  <form role="search" method="get" class="search-form" action="'.get_site_url().'">
                    <p>'.get_field('search_text', 'option').'</p>
                    <input type="search" class="search-field" value="" name="s">
                    <button type="submit" class="search-submit">
                    </button>
                  </form>
                </div>
            </div>
        </div>      
    </div>';
?>