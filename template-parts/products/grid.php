<?php
$images = get_field('image', $product->ID);
if($images) {
    $image = $images['url'];
} else {
    $image = '';
}
echo '<div class="col-12 col-md-4">
    <a class="product" href="'.get_the_permalink($product->ID).'">
        <div class="image" style="background-image: url('.$image.')"></div>
        <h2>'.$product->post_title.'</h2>';
echo wpautop(get_field('description', $product->ID));
echo '</a>
</div>';
?>