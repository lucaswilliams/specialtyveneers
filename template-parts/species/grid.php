<?php
$images = get_field('thumbnail', $species->ID);
if($images) {
    $image = $images['url'];
} else {
    $image = '';
}
echo '<div class="col-12 col-md-6 col-lg-4 col-xl-3">';
    $clickable = get_field('clickable', $species->ID);
    if($clickable) {
        echo '<a class="species" href="'.get_the_permalink($species->ID).'"><div class="info-overlay"></div>';
    } else {
        echo '<div class="species" href="'.get_the_permalink($species->ID).'">';
    }

    echo '<div class="image" style="background-image: url('.$image.')"></div>
        <div class="title"><h2>'.$species->post_title.'</h2></div>';

    if($clickable) {
        echo '</a>';
    } else {
        echo '</div>';
    }

echo '</div>';
?>
