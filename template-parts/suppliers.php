<?php
$suppliers = get_field('suppliers', $post_id);
if($suppliers) {
    ?>
    <div class="col-12">
        <h2 class="text-center">Our Suppliers</h2>
    </div>

    <div class="col-12">
        <div class="row justify-content-center">
            <?php
            foreach($suppliers as $supplier) {
                $supplier_img = get_field('logo', $supplier->ID);
                echo '<a class="col-4 col-md-3 col-lg-2 supplier" href="'.get_field('website', $supplier->ID).'">';
                if($supplier_img) {
                    echo '<img src="'.$supplier_img['url'].'">';
                }
                echo '</a>';
            }
            ?>
        </div>
    </div>
    <?php
}
?>