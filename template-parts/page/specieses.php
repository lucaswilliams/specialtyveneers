<?php $post_id = get_the_ID(); ?>
<div class="row justify-content-center">
    <div class="col-12 col-md-5 introduction">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
    <?php
    $cats = get_categories();
    $spec_list = [];
    foreach($cats as $cat) {
        $spec_list[$cat->name] = get_posts(array(
            'post_type' => 'post',
            'numberposts' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => $cat->taxonomy,
                    'field' => 'slug',
                    'terms' => array($cat->slug),
                    'operator' => 'IN',
                )
            )
        ));
    }
    ?>

    <div class="col-12 species-list">
        <?php
        foreach($spec_list as $cat => $specieses) {
            if(count($specieses) > 0) {
                echo '<h2>' . $cat . '</h2>';
                echo '<div class="row">';
                foreach ($specieses as $species) {
                    include(get_template_directory() . '/template-parts/species/grid.php');
                }
                echo '</div>';
            }
        }
        ?>
    </div>

    <?php include(get_template_directory() . '/template-parts/suppliers.php'); ?>
</div>