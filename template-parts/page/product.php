<?php $post_id = get_the_ID(); ?>
<div class="row justify-content-center">
    <div class="col-12 col-md-8 introduction">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
    <?php
        $cats = get_categories();
        $spec_list = [];
        $ids = get_field('species');
        //var_dump($ids);
        if($ids != null) {
            if (count($cats) > 0) {
                foreach ($cats as $cat) {
                    $spec_list[$cat->name] = get_posts(array(
                        'post_type' => 'post',
                        'numberposts' => -1,
                        'tax_query' => array(
                            array(
                                'taxonomy' => $cat->taxonomy,
                                'field' => 'slug',
                                'terms' => array($cat->slug),
                                'operator' => 'IN',
                            )
                        ),
                        /*'meta_query' => array(
                            array(
                                'key' => 'products',
                                'value' => '"' . $post_id . '"',
                                'compare' => 'LIKE'
                            )
                        ),*/
                        'post__in' => $ids,
                        'orderby' => 'post__in'
                    ));
                }
            }
        } else {
            $spec_list = array();
        }

        $substrates = get_field('substrates', $post_id);
    ?>

    <div class="col-12 species-list">
        <?php
        if(count($spec_list) > 0) {
            foreach ($spec_list as $cat => $specieses) {
                if (count($specieses) > 0) {
                    echo '<h2>' . $cat . '</h2>';
                    echo '<div class="row">';
                    foreach ($specieses as $species) {
                        include(get_template_directory() . '/template-parts/species/grid.php');
                    }
                    echo '</div>';
                }
            }
        }
        ?>
    </div>

    <div class="col-12 species-list">
        <?php
        if (count($substrates) > 0 && is_array($substrates)) {
            echo '<h2>Substrates</h2>';
            echo '<div class="row">';
            foreach ($substrates as $species) {
                include(get_template_directory() . '/template-parts/species/grid.php');
            }
            echo '</div>';
        }
        ?>
    </div>

    <?php
        $milling = get_field('milling_information', $post_id);
        if($milling) {
    ?>
</div>
</div>

<?php
    $millingBanner = get_field('milling_banner', $post_id);
    if($millingBanner) {
        echo '<div class="milling-banner" style="background-image: url('.$millingBanner['url'].');"></div>';
    }
?>

<div class="container">
    <div class="row drygreen">
        <div class="col-12 col-md-6 location">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <?php the_field('dry_mill', $post_id); ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 location">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <?php the_field('green_mill', $post_id); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <?php
        }
    ?>

    <?php include(get_template_directory() . '/template-parts/suppliers.php'); ?>
</div>