<?php $post_id = get_the_ID(); ?>
<div class="row justify-content-center">
    <div class="col-12 col-md-8 introduction">
        <h1><?php the_field('title'); ?></h1>
        <?php the_field('introduction'); ?>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-12 col-xl-5 introduction">
        <h1><?php the_title(); ?></h1>
        <?php echo wpautop(get_the_content()); ?>
    </div>
    <div class="col-12 col-xl-7">
        <div class="row">
        <?php
        $applications = get_posts([
            'post_type' => 'sv_suitability',
            'posts_per_page' => -1,
            'post_status' => 'publish'
        ]);
        foreach($applications as $apply) {
            $images = get_field('icon', $apply->ID);
            echo '<div class="col-12 col-md-6 application">';
            if($images) {
                echo '<img src="'.$images['url'].'">';
            }
            echo $apply->post_title;
            echo '</div>';
        }
        ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 product-list">
        <div class="row">
            <?php
            $product_list = get_posts(['post_type' => 'sv_products', 'posts_per_page' => -1]);
            foreach($product_list as $product) {
                include(get_template_directory().'/template-parts/products/grid.php');
            } ?>
        </div>
    </div>
</div>

<?php include(get_template_directory() . '/template-parts/suppliers.php'); ?>