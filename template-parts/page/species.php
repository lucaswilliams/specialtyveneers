<div class="row">
    <div class="col-12 col-md-3 species-nav">
        <p class="nav-header">Navigation</p>
        <nav class="nav flex-column" id="myTab" role="tablist">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Characteristics</a>
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Environmental</a>
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Technical</a>
        </nav>
        <?php
        $sheet = get_field('spec_sheet_download');
        $spec_sheet = false;
        if($sheet) {
            $spec_sheet = $sheet['url'];
            echo '<a href="'.$spec_sheet.'" class="spec-sheet" target="_blank"><i class="icon-document-filled"></i> Download Spec Sheet</a>';
        }
        ?>
    </div>
    <div class="col-12 col-md-9 species-body">
        <h1><?php the_title(); ?></h1>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="characteristics">
                <?php
                the_field('characteristics');
                ?>
                </div>
                <?php
                if($sheet) {
                    echo '<p class="nav-header">Downloads</p>';
                    echo '<a href="'.$spec_sheet.'" class="spec-sheet" target="_blank"><i class="icon-document-filled"></i> Download Spec Sheet</a>';
                }
                ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <p class="nav-header">Application Suitability</p>
                        <?php
                            $applications = get_field('application_suitability');
                            if(is_array($applications)) {
                                foreach ($applications as $app) {
                                    echo '<p class="suitability">';
                                    $icon = get_field('icon', $app->ID);
                                    if($icon) {
                                        echo '<img src="' . $icon['url'] . '">';
                                    }
                                    echo get_the_title($app->ID) . '</p>';
                                }
                            }
                        ?>
                    </div>
                    <div class="col-12 col-md-6">
                        <p class="nav-header">Products</p>
                        <?php
                        $products = get_field('products');
                        if(is_array($products)) {
                            foreach ($products as $product) {
                                echo '<p><a href="' . get_the_permalink($product->ID) . '">' . get_the_title($product->ID) . '</a></p>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row">
                <?php
                    $i = 1;
                    $environmental = get_field('environmental');
                    if(is_array($environmental)) {
                        foreach ($environmental as $env) {
                            echo '<div class="col-12 col-md-6">';
                            echo '<p><strong>' . $env['heading'] . '</strong><br>' . $env['description'] . '</p>';
                            echo '</div>';

                            if (($i % 2) == 0) {
                                echo '</div><div class="row">';
                            }
                            $i++;
                        }
                    }
                ?>
                </div>
            </div>
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="characteristics">
                <?php the_field('technical'); ?>
                </div>
            </div>
        </div>
    </div>
</div>