<div class="row justify-content-center">
    <div class="col-12 col-lg-7">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</div>