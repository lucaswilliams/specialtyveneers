<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage specialty_veneers
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="page-content">
        <div class="container">
            <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'specialty_veneers' ); ?></h1>
			<div class="page-content">
			    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search, using the icon in the menu at the top?', 'specialty_veneers' ); ?></p>
			</div><!-- .page-content -->
	</div>
</div>

<?php get_footer();
