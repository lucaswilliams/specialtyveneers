<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage specialty_veneers
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="page-content">
    <div class="container">
        <?php
        if(is_single()) {
            $pt = get_post_type();
            switch($pt) {
                case 'sv_products' :
                    get_template_part('template-parts/page/product');
                    break;

                default:
                    get_template_part('template-parts/page/species');
                    break;
            }
        } else {
            get_template_part('template-parts/page/page');
        }
        ?>
    </div>
</div>

<?php get_footer();
