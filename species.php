<?php
/**
 * Template name: Species
 */

get_header();
?>

    <div class="page-content">
        <div class="container">
            <?php
            get_template_part('template-parts/page/specieses');
            ?>
        </div>
    </div>

<?php
get_footer();
?>