<?php
/**
 * Template name: Exports
 */

get_header();
?>

    <div class="page-content">
        <div class="container">
            <?php
            get_template_part('template-parts/page/page');
            ?>
        </div>
    </div>

<?php
get_footer();
?>