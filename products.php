<?php
/**
 * Template name: Products
 */

get_header();
?>

<div class="page-content">
    <div class="container">
        <?php
            get_template_part('template-parts/page/products');
        ?>
    </div>
</div>

<?php
get_footer();
?>



