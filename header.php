<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage specialty_veneers
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

<!-- Custom Favicon -->
<link rel="shortcut icon" href="<?php echo get_theme_file_uri('images/favicon.jpg'); ?>"/>
</head>

<body <?php body_class(); ?>>
<?php if ( has_nav_menu( 'top' ) ) : ?>
    <div class="navigation-top">
        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
    </div><!-- .navigation-top -->
<?php endif; ?>

<?php
$banner = get_field('banner');
$banner_style = get_field('banner_style');
switch($banner_style) {
    case 1 : $bstyle = 'tall';
    $home = true;
    break;

    case 2 : $bstyle = 'short';
    $home = false;
    break;

    default : $bstyle = '';
    $home = false;
    break;
}

if(is_search()) {
    $banner = null;
}

if(is_array($banner)) {
    switch (count($banner)) {
        case 1:
            echo '<div class="featured-image '.$bstyle.'" style="background-image: url('.$banner[0]['image']['url'].')">';
            if(strlen($banner[0]['caption']) > 0) {
                if(!$home) {
                    echo '<div class="container">';
                }
                echo '<div class="banner-caption">'.$banner[0]['caption'].'</div>';
                if($home) {
                    echo '</div>';
                }
            }
            echo '</div>';
            break;

        default:
            echo '<div id="featuredCarousel" class="carousel slide featured-image '.$bstyle.'" data-ride="carousel">
            <ol class="carousel-indicators">';
            for ($i = 0; $i < count($banner); $i++) {
                echo '<li data-target="#featuredCarousel" data-slide-to="'.$i.'" class="' . ($i == 0 ? 'active' : '') . '"></li>';
            }
            echo '</ol>
            <div class="carousel-inner">';
            for ($i = 0; $i < count($banner); $i++) {
                echo '<div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="background-image: url(' . $banner[$i]['image']['url'] . '">';
                if(strlen($banner[$i]['caption']) > 0) {
                    if(!$home) {
                        echo '<div class="container">';
                    }
                    echo '<div class="banner-caption"><p>'.$banner[$i]['caption'].'</p></div>';
                    if(!$home) {
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
            echo '</div></div>';
            break;
    }
}
?>