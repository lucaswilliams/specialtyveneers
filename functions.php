<?php
/**
 * Specialty_Veneers functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage specialty_veneers
 * @since 1.0
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function specialty_veneers_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/specialty_veneers
	 * If you're building a theme based on Specialty_Veneers, use a find and replace
	 * to change 'specialty_veneers' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'specialty_veneers' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'specialty_veneers-featured-image', 2000, 1200, true );
	add_image_size( 'facebook', 1200, 630, true );

	add_image_size( 'specialty_veneers-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'specialty_veneers' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'css/editor-style.css', specialty_veneers_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'specialty_veneers' ),
				'file' => 'images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'specialty_veneers' ),
				'file' => 'images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'specialty_veneers' ),
				'file' => 'images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'specialty_veneers' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'specialty_veneers' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Specialty_Veneers array of starter content.
	 *
	 * @since Specialty_Veneers 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'specialty_veneers_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );

    register_post_type('sv_products', [
        'labels' => [
            'name' => 'Products',
            'singular_name' => 'Product',
            'add_new' => 'Add new product',
            'add_new_item' => 'Add new product',
            'edit_item' => 'Edit product',
            'new_item' => 'New product'
        ],
        'public' => true,
        'has_archive' => true,
        'supports' => ['title', 'editor'],
        'menu_icon' => 'dashicons-tickets-alt',
        'rewrite' => ['slug' => 'product', 'with_front' => false]
    ]);

    register_post_type('sv_suitability', [
        'labels' => [
            'name' => 'Suitability',
            'singular_name' => 'Suitability',
            'add_new' => 'Add new suitability',
            'add_new_item' => 'Add new suitability',
            'edit_item' => 'Edit suitability',
            'new_item' => 'New suitability'
        ],
        'public' => true,
        'publicly_queryable' => false,
        'has_archive' => true,
        'supports' => ['title', 'editor'],
        'menu_icon' => 'dashicons-networking'
    ]);

    register_post_type('sv_substrates', [
        'labels' => [
            'name' => 'Substrates',
            'singular_name' => 'Substrate',
            'add_new' => 'Add new substrate',
            'add_new_item' => 'Add new substrate',
            'edit_item' => 'Edit substrate',
            'new_item' => 'New substrate'
        ],
        'public' => true,
        'publicly_queryable' => false,
        'has_archive' => true,
        'supports' => ['title', 'editor'],
        'menu_icon' => 'dashicons-schedule'
    ]);
    
    register_post_type('sv_suppliers', [
        'labels' => [
            'name' => 'Suppliers',
            'singular_name' => 'Supplier',
            'add_new' => 'Add new supplier',
            'add_new_item' => 'Add new supplier',
            'edit_item' => 'Edit supplier',
            'new_item' => 'New supplier'
        ],
        'public' => true,
        'publicly_queryable' => false,
        'has_archive' => false,
        'supports' => ['title', 'editor'],
        'menu_icon' => 'dashicons-store',
        'rewrite' => ['slug' => 'supplier', 'with_front' => false]
    ]);
}
add_action( 'after_setup_theme', 'specialty_veneers_setup' );

function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function specialty_veneers_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( specialty_veneers_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Specialty_Veneers content width of the theme.
	 *
	 * @since Specialty_Veneers 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'specialty_veneers_content_width', $content_width );
}
add_action( 'template_redirect', 'specialty_veneers_content_width', 0 );

/**
 * Register custom fonts.
 */
function specialty_veneers_fonts_url() {
	$fonts_url = '';
	return esc_url_raw( 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700' );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function specialty_veneers_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'specialty_veneers-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'specialty_veneers_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function specialty_veneers_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'specialty_veneers' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'specialty_veneers' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'specialty_veneers' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'specialty_veneers' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'specialty_veneers' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'specialty_veneers' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'specialty_veneers_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function specialty_veneers_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'specialty_veneers' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'specialty_veneers_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Specialty_Veneers 1.0
 */
function specialty_veneers_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'specialty_veneers_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function specialty_veneers_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'specialty_veneers_pingback_header' );

/**
 * Enqueue scripts and styles.
 */
function specialty_veneers_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'specialty_veneers-fonts', specialty_veneers_fonts_url(), array(), null );
	wp_enqueue_style( 'specialty_veneers-rt-glyph', get_theme_file_uri('css/rt-glyph.css') );

	// Theme stylesheet.
    wp_enqueue_style( 'specialty_veneers-bootstrap-css', get_theme_file_uri('css/bootstrap.css') );

    // Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'specialty_veneers-ie9', get_theme_file_uri( '/css/ie9.css' ), array( 'specialty_veneers-style' ), '1.0' );
		wp_style_add_data( 'specialty_veneers-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'specialty_veneers-ie8', get_theme_file_uri( '/css/ie8.css' ), array( 'specialty_veneers-style' ), '1.0' );
	wp_style_add_data( 'specialty_veneers-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'specialty_veneers-skip-link-focus-fix', get_theme_file_uri( '/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$twentyseventeen_l10n = array(
		'quote'          => specialty_veneers_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'specialty_veneers-navigation', get_theme_file_uri( '/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$specialty_veneers_l10n['expand']         = __( 'Expand child menu', 'specialty_veneers' );
		$specialty_veneers_l10n['collapse']       = __( 'Collapse child menu', 'specialty_veneers' );
	}

	wp_enqueue_script( 'specialty_veneers-global', get_theme_file_uri( '/js/global.js' ), array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );
	wp_enqueue_script( 'jquery-bootstrap', get_theme_file_uri( '/js/bootstrap.min.js' ), array( 'jquery' ), '4.0.0', true );

	wp_localize_script( 'specialty_veneers-skip-link-focus-fix', 'specialty_veneersScreenReaderText', $specialty_veneers_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'specialty_veneers_scripts' );

function specialty_veneers_custom_css() {
    wp_enqueue_style( 'specialty_veneers-style', get_theme_file_uri('css/custom.css') );
}

add_action( 'wp_enqueue_scripts', 'specialty_veneers_custom_css', 500000);

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function specialty_veneers_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'specialty_veneers_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function specialty_veneers_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'specialty_veneers_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function specialty_veneers_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'specialty_veneers_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Specialty_Veneers 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function specialty_veneers_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'specialty_veneers_front_page_template' );

function insert_fb_in_head() {
	global $post;
	if ( !is_singular()) {
		//if it is not a post or a page
		return;
	}
	echo '<meta property="og:title" content="' . get_the_title() . '"/>';
	echo '<meta property="og:type" content="article"/>';
	echo '<meta property="og:url" content="' . get_permalink() . '"/>';
	echo '<meta property="og:site_name" content="'.get_bloginfo('name').'"/>';
	if(function_exists('get_field')) {
		$image = get_field('featured_image');
	} else {
		$image = false;
	}
	if($image) {
	    echo '<meta property="og:image" content="' . esc_attr( $image['sizes']['facebook'] ) . '"/>';
		echo '<meta property="og:image:secure_url" content="' . esc_attr( $image['sizes']['facebook'] ) . '"/>';
	} else {
		$default_image = get_theme_file_uri('images/logos/logo.png'); //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '"/>';
		echo '<meta property="og:image:secure_url" content="' . $default_image . '"/>';
	}
	echo "\n";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

/**
 * Checks to see if we're on the homepage or not.
 */
function specialty_veneers_is_frontpage() {
    return ( is_front_page() && ! is_home() );
}

function specialty_veneers_time_link() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
    if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
        $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
    }

    $time_string = sprintf( $time_string,
        get_the_date( DATE_W3C ),
        get_the_date(),
        get_the_modified_date( DATE_W3C ),
        get_the_modified_date()
    );

    // Wrap the time string in a link, and preface it with 'Posted on'.
    return sprintf(
    /* translators: %s: post date */
        __( '<span class="screen-reader-text">Posted on</span> %s', 'specialty_veneers' ),
        '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
    );
}

function specialty_veneers_get_svg( $args = array() ) {
    // Make sure $args are an array.
    if ( empty( $args ) ) {
        return __( 'Please define default parameters in the form of an array.', 'specialty_veneers' );
    }

    // Define an icon.
    if ( false === array_key_exists( 'icon', $args ) ) {
        return __( 'Please define an SVG icon filename.', 'specialty_veneers' );
    }

    // Set defaults.
    $defaults = array(
        'icon'        => '',
        'title'       => '',
        'desc'        => '',
        'fallback'    => false,
    );

    // Parse args.
    $args = wp_parse_args( $args, $defaults );

    // Set aria hidden.
    $aria_hidden = ' aria-hidden="true"';

    // Set ARIA.
    $aria_labelledby = '';

    /*
       * Twenty Seventeen doesn't use the SVG title or description attributes; non-decorative icons are described with .screen-reader-text.
       *
       * However, child themes can use the title and description to add information to non-decorative SVG icons to improve accessibility.
       *
       * Example 1 with title: <?php echo specialty_veneers_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ) ) ); ?>
       *
       * Example 2 with title and description: <?php echo specialty_veneers_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ), 'desc' => __( 'This is the description', 'textdomain' ) ) ); ?>
       *
       * See https://www.paciellogroup.com/blog/2013/12/using-aria-enhance-svg-accessibility/.
       */
    if ( $args['title'] ) {
        $aria_hidden     = '';
        $unique_id       = uniqid();
        $aria_labelledby = ' aria-labelledby="title-' . $unique_id . '"';

        if ( $args['desc'] ) {
            $aria_labelledby = ' aria-labelledby="title-' . $unique_id . ' desc-' . $unique_id . '"';
        }
    }

    // Begin SVG markup.
    $svg = '<svg class="icon icon-' . esc_attr( $args['icon'] ) . '"' . $aria_hidden . $aria_labelledby . ' role="img">';

    // Display the title.
    if ( $args['title'] ) {
        $svg .= '<title id="title-' . $unique_id . '">' . esc_html( $args['title'] ) . '</title>';

        // Display the desc only if the title is already set.
        if ( $args['desc'] ) {
            $svg .= '<desc id="desc-' . $unique_id . '">' . esc_html( $args['desc'] ) . '</desc>';
        }
    }

    /*
       * Display the icon.
       *
       * The whitespace around `<use>` is intentional - it is a work around to a keyboard navigation bug in Safari 10.
       *
       * See https://core.trac.wordpress.org/ticket/38387.
       */
    $svg .= ' <use href="#icon-' . esc_html( $args['icon'] ) . '" xlink:href="#icon-' . esc_html( $args['icon'] ) . '"></use> ';

    // Add some markup to use as a fallback for browsers that do not support SVGs.
    if ( $args['fallback'] ) {
        $svg .= '<span class="svg-fallback icon-' . esc_attr( $args['icon'] ) . '"></span>';
    }

    $svg .= '</svg>';

    return $svg;
}

function specialty_veneers_posted_on() {

    // Get the author name; wrap it in a link.
    $byline = sprintf(
    /* translators: %s: post author */
        __( 'by %s', 'specialty_veneers' ),
        '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
    );

    // Finally, let's write all of this to the page.
    echo '<span class="posted-on">' . specialty_veneers_time_link() . '</span><span class="byline"> ' . $byline . '</span>';
}

function specialty_veneers_edit_link() {
    edit_post_link(
        sprintf(
        /* translators: %s: Name of current post */
            __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'specialty_veneers' ),
            get_the_title()
        ),
        '<span class="edit-link">',
        '</span>'
    );
}

function specialty_veneers_entry_footer() {

    /* translators: used between list items, there is a space after the comma */
    $separate_meta = __( ', ', 'specialty_veneers' );

    // Get Categories for posts.
    $categories_list = get_the_category_list( $separate_meta );

    // Get Tags for posts.
    $tags_list = get_the_tag_list( '', $separate_meta );

    // We don't want to output .entry-footer if it will be empty, so make sure its not.
    if ( ( ( specialty_veneers_categorized_blog() && $categories_list ) || $tags_list ) || get_edit_post_link() ) {

        echo '<footer class="entry-footer">';

        if ( 'post' === get_post_type() ) {
            if ( ( $categories_list && specialty_veneers_categorized_blog() ) || $tags_list ) {
                echo '<span class="cat-tags-links">';

                // Make sure there's more than one category before displaying.
                if ( $categories_list && specialty_veneers_categorized_blog() ) {
                    echo '<span class="cat-links">' . specialty_veneers_get_svg( array( 'icon' => 'folder-open' ) ) . '<span class="screen-reader-text">' . __( 'Categories', 'specialty_veneers' ) . '</span>' . $categories_list . '</span>';
                }

                if ( $tags_list && ! is_wp_error( $tags_list ) ) {
                    echo '<span class="tags-links">' . specialty_veneers_get_svg( array( 'icon' => 'hashtag' ) ) . '<span class="screen-reader-text">' . __( 'Tags', 'specialty_veneers' ) . '</span>' . $tags_list . '</span>';
                }

                echo '</span>';
            }
        }

        specialty_veneers_edit_link();

        echo '</footer> <!-- .entry-footer -->';
    }
}

function specialty_veneers_categorized_blog() {
    $category_count = get_transient( 'specialty_veneers_categories' );

    if ( false === $category_count ) {
        // Create an array of all the categories that are attached to posts.
        $categories = get_categories( array(
            'fields'     => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number'     => 2,
        ) );

        // Count the number of categories that are attached to the posts.
        $category_count = count( $categories );

        set_transient( 'specialty_veneers_categories', $category_count );
    }

    // Allow viewing case of 0 or 1 categories in post preview.
    if ( is_preview() ) {
        return true;
    }

    return $category_count > 1;
}

function hide_update_notice_to_all_but_admin_users()
{
    //$current_user = wp_get_current_user();

    //if (strcmp($current_user->user_login, 'RoundTableStudio') != 0) {
        remove_action( 'admin_notices', 'update_nag', 3 );

        echo '<style>.plugin-count, .update-count, .update-plugins, .yoast-issue-counter { display: none !important; }</style>';
    //}
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

add_filter( 'the_password_form', 'flyingfish_custom_post_password_msg' );

/**
 * Add a message to the password form.
 *
 * @wp-hook the_password_form
 * @param   string $form
 * @return  string
 */
function flyingfish_custom_post_password_msg( $form )
{
	// No cookie, the user has not sent anything until now.
	if ( ! isset ( $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] ))
        return $form;
	
	echo $_SERVER['HTTP_REFERER'];

	echo 'ref:'.wp_get_referer().'<br />pl:'.get_permalink().'<br />';
    if(strcmp(wp_get_referer(), get_permalink()) == 0) {
		// Translate and escape.
		$msg = esc_html__( 'Sorry, the password you have entered is incorrect.', 'your_text_domain' );

		// We have a cookie, but it doesn’t match the password.
		$msg = "<p class='custom-password-message'>$msg</p>";
	}

    return $msg . $form;
}

function specialty_veneers_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Species';
    $submenu['edit.php'][5][0] = 'Species';
    $submenu['edit.php'][10][0] = 'Add Species';
    $submenu['edit.php'][16][0] = 'Species Tags';
}
function specialty_veneers_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Species';
    $labels->singular_name = 'Species';
    $labels->add_new = 'Add Species';
    $labels->add_new_item = 'Add Species';
    $labels->edit_item = 'Edit Species';
    $labels->new_item = 'Species';
    $labels->view_item = 'View Species';
    $labels->search_items = 'Search Species';
    $labels->not_found = 'No Species found';
    $labels->not_found_in_trash = 'No Species found in Trash';
    $labels->all_items = 'All Species';
    $labels->menu_name = 'Species';
    $labels->name_admin_bar = 'Species';
}

add_action( 'admin_menu', 'specialty_veneers_change_post_label' );
add_action( 'init', 'specialty_veneers_change_post_object' );

acf_add_options_page([
    'page_title' => 'Site Settings',
    'capability' => 'edit_posts',
]);

function bidirectional_acf_update_value( $value, $post_id, $field  ) {
    // set the field names to check
    // for each post
    $name_a = 'species'; //$field_a['name'];
    $name_b = 'products'; //$field_b['name'];

    if ($name_a != $field['name']) {
        // this is side b, swap the value
        $temp = $name_a;
        $name_a = $name_b;
        $name_b = $temp;
    }

    $key_b = get_acf_field_keys($name_b);

    // get the old value from the current post
    // compare it to the new value to see
    // if anything needs to be updated
    // use get_post_meta() to a avoid conflicts
    $old_values = get_post_meta($post_id, $name_a, true);
    // make sure that the value is an array
    if (!is_array($old_values)) {
        if (empty($old_values)) {
            $old_values = array();
        } else {
            $old_values = array($old_values);
        }
    }
    // set new values to $value
    // we don't want to mess with $value
    $new_values = $value;
    // make sure that the value is an array
    if (!is_array($new_values)) {
        if (empty($new_values)) {
            $new_values = array();
        } else {
            $new_values = array($new_values);
        }
    }

    // get differences
    // array_diff returns an array of values from the first
    // array that are not in the second array
    // this gives us lists that need to be added
    // or removed depending on which order we give
    // the arrays in

    // this line is commented out, this line should be used when setting
    // up this filter on a new site. getting values and updating values
    // on every relationship will cause a performance issue you should
    // only use the second line "$add = $new_values" when adding this
    // filter to an existing site and then you should switch to the
    // first line as soon as you get everything updated
    // in either case if you have too many existing relationships
    // checking end updated every one of them will more then likely
    // cause your updates to time out.
    //$add = array_diff($new_values, $old_values);
    $add = $new_values;
    $delete = array_diff($old_values, $new_values);

    // reorder the arrays to prevent possible invalid index errors
    $add = array_values($add);
    $delete = array_values($delete);

    if (!count($add) && !count($delete)) {
        // there are no changes
        // so there's nothing to do
        return $value;
    }

    // do deletes first
    // loop through all of the posts that need to have
    // the recipricol relationship removed
    for ($i=0; $i<count($delete); $i++) {
        $related_values = get_post_meta($delete[$i], $name_b, true);
        if (!is_array($related_values)) {
            if (empty($related_values)) {
                $related_values = array();
            } else {
                $related_values = array($related_values);
            }
        }
        // we use array_diff again
        // this will remove the value without needing to loop
        // through the array and find it
        $related_values = array_diff($related_values, array($post_id));
        // insert the new value
        update_post_meta($delete[$i], $name_b, $related_values);
        // insert the acf key reference, just in case
        update_post_meta($delete[$i], '_'.$name_b, $key_b);
    }

    // do additions, to add $post_id
    for ($i=0; $i<count($add); $i++) {
        $related_values = get_post_meta($add[$i], $name_b, true);
        if (!is_array($related_values)) {
            if (empty($related_values)) {
                $related_values = array();
            } else {
                $related_values = array($related_values);
            }
        }
        if (!in_array($post_id, $related_values)) {
            // add new relationship if it does not exist
            $related_values[] = $post_id;
        }
        // update value
        update_post_meta($add[$i], $name_b, $related_values);
        // insert the acf key reference, just in case
        //update_post_meta($add[$i], '_'.$name_b, $key_b);
    }

    return $value;
}

function get_acf_field_keys( $custom_field_slug = '' ) {
    $result = array();
    $meta_key_start = 'field_';
    $acf_args = array(
        'post_type'		=> 'acf'
    );

    if ( $custom_field_slug !== '' ) {
        $acf_args[ 'name' ] = $custom_field_slug;
    }

    $acf_query = new WP_Query( $acf_args );

    if ( $acf_query->have_posts() ) {
        while ( $acf_query->have_posts() ) {
            $acf_query->the_post();
            $meta_values = get_post_meta( get_the_id() );
            foreach ( $meta_values as $meta_key => $meta_value ) {
                if ( substr( $meta_key, 0, strlen( $meta_key_start ) ) === $meta_key_start ) {
                    $meta_value_array = unserialize( $meta_value[0] );
                    $result[ $meta_value_array['name'] ] = $meta_key;
                }
            }
        }
    }

    wp_reset_postdata();

    if ( empty( $result ) ) {
        $result = false;
    }

    return $result;
}

add_filter('acf/update_value/name=species', 'bidirectional_acf_update_value', 10, 3);
add_filter('acf/update_value/name=products', 'bidirectional_acf_update_value', 10, 3);

add_filter("gform_confirmation_anchor", create_function("","return false;"));