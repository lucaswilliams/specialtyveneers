<?php
/**
 * Template name: Contact
 */

get_header();
?>

    <iframe src="https://snazzymaps.com/embed/93492" width="100%" height="600px" style="border:none;" id="snazzymap"></iframe>

    <div class="container contacts">
        <div class="row">
            <?php
            $locations = get_field('locations');
            foreach($locations as $location) {
                echo '<div class="col-12 col-md-6 location">';
                echo '<div class="row justify-content-center">';
                echo '<div class="col-12 col-md-8">';
                $images = $location['image'];
                if($images) {
                    echo '<img src="'.$images['url'].'">';
                }
                echo '<h1>'.$location['name'].'</h1>';
                echo '<p>'.$location['address'].'</p>';
                echo '<p>Ph: '.$location['phone'].'<br>Fax: '.$location['fax'].'</p>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            ?>
        </div>
    </div>

<?php
get_footer();